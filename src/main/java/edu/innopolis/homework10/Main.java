package edu.innopolis.homework10;

public class Main {
    public static void main(String[] args) {
        User validUser = new User("Kirill", "4vXsjQW2", 20);
        User passwordLength_NotValidUser = new User("Alice", "a", 23);
        User passwordEmpty_NotValidUser = new User("Alice", "", 23);
        User ageNot_ValidUser = new User("Bob", "WxxV76ps13", 102);
        User nameNot_ValidUser = new User("Vik", "QWERTY!234", 34);
        Validator validator = new Validator();
        System.out.println("validUser");
        validator.validate(validUser);
        System.out.println("passwordLength_NotValidUser");
        validator.validate(passwordLength_NotValidUser);
        System.out.println("passwordEmpty_NotValidUser");
        validator.validate(passwordEmpty_NotValidUser);
        System.out.println("ageNot_ValidUser");
        validator.validate(ageNot_ValidUser);
        System.out.println("nameNot_ValidUser");
        validator.validate(nameNot_ValidUser);
    }
}
