package edu.innopolis.homework10;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

//https://www.developer.com/design/seven-ways-to-refactor-java-switch-statements/
//'Implementing the Command Design Pattern'
public class ValidatorManager {
    private static final Map<String, Command> commands;

    static {
        final Map<String, Command> commandMap = new HashMap<>();
        //Предположим что @Max и @Min будут работать только с потомками класса Number
        commandMap.put(Max.class.getName(), (object, field) -> {
            if (field.getType().equals(Integer.TYPE)) {
                Integer value = (Integer) field.get(object);
                Max max = field.getAnnotation(Max.class);
                Integer maxValue = max.value();
                if (value > maxValue) {
                    throw new IllegalArgumentException("'" + field.getName() + "'" + " - value is above maximum");
                }
            } else {
                throw new IllegalArgumentException("@Max - Annotation not comparable with this datatype ");
            }
        });
        commandMap.put(Min.class.getName(), (object, field) -> {
            if (field.getType().equals(Integer.TYPE)) {
                Integer value = (Integer) field.get(object);
                Min min = field.getAnnotation(Min.class);
                Integer minValue = min.value();
                if (value < minValue) {
                    throw new IllegalArgumentException("'" + field.getName() + "'" + " - value is below minimum");
                }
            } else {
                throw new IllegalArgumentException("@Min - Annotation not comparable with this datatype");
            }
        });
        //Аналогично предположим что @MinMaxLength работает только со строками
        commandMap.put(MinMaxLength.class.getName(), (object, field) -> {
            if (field.getType() == String.class) {
                String strValue = (String) field.get(object);
                MinMaxLength minMaxLength = field.getAnnotation(MinMaxLength.class);
                int minLength = minMaxLength.minValue();
                int maxLength = minMaxLength.maxValue();
                if (!(minLength <= strValue.length() && strValue.length() < maxLength)) {
                    throw new IllegalArgumentException("'" + field.getName() + "'" + " - string length is not correct");
                }
            } else {
                throw new IllegalArgumentException("@MinMaxLength - Annotation not comparable with this datatype");
            }
        });
        //Предположим что наша аннотация @NotEmpty будет работать только со строками
//это позволит точно проверять поле через метод isEmpty()
        commandMap.put(NotEmpty.class.getName(), (object, field) -> {
            if (field.getType() == String.class) {
                String strValue = (String) field.get(object);
                if (strValue.isEmpty()) {
                    throw new IllegalArgumentException("'" + field.getName() + "'" + " - field is empty");
                }
            } else {
                throw new IllegalArgumentException("@NotEmpty - Annotation not comparable with this datatype");
            }
        });
        commands = Collections.unmodifiableMap(commandMap);
    }

    public static void annotationValidate(Object obj, Field field, Annotation annotation) throws IllegalAccessException {
        Class<? extends Annotation> type = annotation.annotationType();
        String annotationName = type.getName();
        Command command = commands.get(annotationName);
        if (command == null) {
            throw new IllegalArgumentException("Wrong annotation name");
        } else command.validate(obj, field);
    }
}
