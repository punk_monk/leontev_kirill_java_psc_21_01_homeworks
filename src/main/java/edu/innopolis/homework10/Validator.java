package edu.innopolis.homework10;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import static edu.innopolis.homework10.ValidatorManager.annotationValidate;

public class Validator {

    public void validate(Object obj) {
        Class<?> cl = obj.getClass();
        Field[] fields = cl.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation : annotations) {
                try {
                    annotationValidate(obj ,field, annotation);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    System.out.println(e.getLocalizedMessage());
                }
            }
        }
    }
}
