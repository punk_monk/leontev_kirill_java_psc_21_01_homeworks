package edu.innopolis.homework10;

import java.lang.reflect.Field;

public interface Command {
    void validate(Object object, Field field) throws IllegalAccessException;
}
