package edu.innopolis.homework10;

public class User {
    @MinMaxLength(minValue = 5, maxValue = 20)
    String name;

    @NotEmpty
    @MinMaxLength(minValue = 7)
    String password;

    @Min(value = 18)
    @Max(value = 99)
    int age;

    public User(String name, String password, int age) {
        this.name = name;
        this.password = password;
        this.age = age;
    }
}
