package edu.innopolis.homework5;

public class BadEmailException extends IllegalArgumentException{
    //Используем такую конструкцию, чтобы не писать разные сообщения для такого узкого исключения
    @Override
    public String getMessage() {
        return "Неверный формат почты";
    }
}

