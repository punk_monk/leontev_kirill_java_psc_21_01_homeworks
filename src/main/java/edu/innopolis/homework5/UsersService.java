package edu.innopolis.homework5;

public interface UsersService {
    //'throws' в интерфейсе говорит о том, что реализация возможно вызовет эти исключения
    void signUp(String email, String password) throws BadEmailException, BadPasswordException;
    void signIn(String email, String password) throws UserNotFoundException;
}

