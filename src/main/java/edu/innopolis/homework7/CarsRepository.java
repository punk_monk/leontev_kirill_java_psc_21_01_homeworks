package edu.innopolis.homework7;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CarsRepository {
    private String fileName;
    private List<Car> cars;
    private Pattern pattern;
    private Matcher matcher;

    //Мне кажется это не очень хорошее решение - давай пользователю работать с файлом
    //зная только его имя - что если понадобится URL?
    public CarsRepository(String fileName) {
        this.fileName = fileName;
    }

    public final Function<String, Car> userMapFunction = line -> {
        pattern = Pattern.compile("\\[([^]]+)\\]");
        matcher = pattern.matcher(line);
        List<String> list = new ArrayList<>();
        while (matcher.find()) {
            //Matcher вернет нам кусок в формате [xxx], поэтому пишем 1 чтобы взять только xxx
            list.add(matcher.group(1));
        }
        long carID = Long.parseLong(list.get(0));
        String modelName = list.get(1);
        String color = list.get(2);
        double mileage = Double.parseDouble(list.get(3));
        double price = Double.parseDouble(list.get(4));

        return new Car(carID, modelName, color, mileage, price);
    };

    //Огромная конструкция ниже - эксперимент, как зная только имя файла прочитать его из папки resources,
    //не изменяя исходный код
    public List<Car> findAll() {
        try (BufferedReader reader = new BufferedReader(
                new FileReader(
                        this.getClass().getClassLoader().getResource(fileName).getFile())
        )
        ) {
            cars = reader.lines().map(userMapFunction).collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return cars;
    }
}
