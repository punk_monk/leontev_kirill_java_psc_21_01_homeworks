package edu.innopolis.homework7;

import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {
    //Реализация не моя - по запросу "distinct by property" везде встречается этот код
    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepository("cars.txt");
        System.out.println("Номера всех автомобилей, имеющих черный цвет или нулевой пробег");
        //Множественное условие filter() через оператор - ок?
        carsRepository.findAll().
                stream().
                filter(car -> car.getColor().equals("Black") || car.getMileage() == 0)
                .mapToLong(Car::getCarID).
                distinct().
                forEach(System.out::println);
        System.out.println("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.");
        //Не совсем нравится такое решение определения числа в диапазоне - есть ли альтернативы?
        System.out.println(
                carsRepository.
                        findAll().
                        stream()
                        .filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                        .collect(Collectors.toList()).
                        stream().
                        filter(distinctByKey(Car::getModelName)).
                        count());
        System.out.println("Вывести цвет автомобиля с минимальной стоимостью");
        System.out.println(
                carsRepository.
                        findAll()
                        .stream()
                        .min(Comparator.comparing(Car::getPrice))
                        .get().getColor());
        System.out.println("Среднюю стоимость Camry");
        System.out.println(
                carsRepository.findAll().
                        stream().
                        filter(car -> car.getModelName().equals("Camry")).
                        mapToDouble(Car::getPrice).
                        average().
                        getAsDouble());
    }
}
