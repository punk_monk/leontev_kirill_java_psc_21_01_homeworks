package edu.innopolis.homework7;

public class Car {
    private long carID;
    private String modelName;
    private String color;
    private double mileage;
    private double price;

    public Car(long carID, String modelName, String color, double mileage, double price) {
        this.carID = carID;
        this.modelName = modelName;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public long getCarID() {
        return carID;
    }

    public String getModelName() {
        return modelName;
    }

    public String getColor() {
        return color;
    }

    public double getMileage() {
        return mileage;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carID=" + carID +
                ", modelName='" + modelName + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}';
    }
}
