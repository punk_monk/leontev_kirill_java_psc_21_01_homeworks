package edu.innopolis.attestations.reflection_01;

import java.io.Serializable;

public class User implements Serializable {
    int age;
    long salary;
    byte id;
    String name;
    boolean isMan;
    double height;

    public User(int age, long salary, byte id, String name, boolean isMan, double height) {
        this.age = age;
        this.salary = salary;
        this.id = id;
        this.name = name;
        this.isMan = isMan;
        this.height = height;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMan() {
        return isMan;
    }

    public void setMan(boolean man) {
        isMan = man;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
