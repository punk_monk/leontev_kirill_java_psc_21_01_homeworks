package edu.innopolis.attestations.reflection_01;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        User user = new User(43, 1000, (byte) 1, "John", true, 6.05);
        Set<String> fieldsToCleanup = new HashSet<>(Arrays.asList("age", "isMan", "height"));
        Set<String> fieldsToOutput = new HashSet<>(Arrays.asList("age", "salary", "id", "isMan", "height", "name"));
        Modifier modifier = new Modifier();
        modifier.cleanup(user, fieldsToCleanup, fieldsToOutput);
    }
}
