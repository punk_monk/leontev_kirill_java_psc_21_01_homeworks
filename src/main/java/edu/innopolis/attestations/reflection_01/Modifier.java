package edu.innopolis.attestations.reflection_01;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Modifier {
    private static final int NUMBER_DEFAULT_VALUE = 0;
    private static final boolean BOOLEAN_DEFAULT_VALUE = false;
    private static final char CHAR_DEFAULT_VALUE = ' ';
    Set<Type> primitiveNumberTypes = new HashSet<>(Arrays.asList(Integer.TYPE, Long.TYPE, Double.TYPE, Short.TYPE, Double.TYPE));
    
    public void cleanup(Object object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput) {
        Class<?> cl = object.getClass();
        if (cl.getSuperclass().equals(Map.class)) {
            object = cleanupMapFields(object, fieldsToCleanup);
            outputMapFields(object, fieldsToOutput);
        } else {
            object = cleanupFields(object, fieldsToCleanup);
            outputFields(object, fieldsToOutput);
        }
    }

    private <T extends Map<String, ?>> Object cleanupMapFields(Object t, Set<String> fieldsToCleanup) {
        //Делаем hard copy, чтобы создать клон оригинала, в котором можно удалять поля без последствий для первого
        T tempT = (T) deepClone(t);
        for (String fl : fieldsToCleanup) {
            if (tempT.containsKey(fl)) {
                tempT.remove(fl);
            } else throw new IllegalArgumentException("Key not found");
        }
        return tempT;
    }

    private <T extends Map<String, ?>> void outputMapFields(Object t, Set<String> fieldsToOutput) {
        T tempT = (T) deepClone(t);
        for (String fl : fieldsToOutput) {
            if (tempT.containsKey(fl)) {
                System.out.println(tempT.get(fl));
            } else throw new IllegalArgumentException("Key not found");
        }
    }

    //https://www.journaldev.com/17129/java-deep-copy-object
    public static Object deepClone(Object object) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(object);
            ByteArrayInputStream bais = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
            ObjectInputStream objectInputStream = new ObjectInputStream(bais);
            return objectInputStream.readObject();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Object cleanupFields(Object object, Set<String> fieldsToCleanup) {
        Class<?> cl = object.getClass();
        Object tempObject = deepClone(object);
        for (String fl : fieldsToCleanup) {
            try {
                Field field = cl.getDeclaredField(fl);
                Class<?> fieldClass = field.getType();
                if (fieldClass.isPrimitive()) {
                    Object defaultValue = getPrimitiveDefault(fieldClass);
                    field.set(tempObject, defaultValue);
                } else {
                    field.set(tempObject, null);
                }
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new IllegalArgumentException("Field not found");
            }
        }
        return tempObject;
    }

    private Object getPrimitiveDefault(Class<?> fieldClass) {
        if (primitiveNumberTypes.contains(fieldClass)) {
            return NUMBER_DEFAULT_VALUE;
        }
        if (fieldClass.equals(Character.TYPE)) {
            return CHAR_DEFAULT_VALUE;
        }
        if (fieldClass.equals(Boolean.TYPE)) {
            return BOOLEAN_DEFAULT_VALUE;
        }
        else return null;
    }

    private void outputFields(Object object, Set<String> fieldsToOutput) {
        Class<?> cl = object.getClass();
        StringBuilder stringBuilder = new StringBuilder();
        for (String fl : fieldsToOutput) {
            try {
                Field field = cl.getDeclaredField(fl);
                String name = field.getName();
                String value = String.valueOf(field.get(object));
                stringBuilder.append(name).append(" ").append(value).append("\n");
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new IllegalArgumentException("Field not found");
            }
        }
        System.out.println(stringBuilder);
    }

}
