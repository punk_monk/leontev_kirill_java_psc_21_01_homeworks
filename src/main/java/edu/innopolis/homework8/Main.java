package edu.innopolis.homework8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        String filename = "threads.txt";
        ExecutorService executor = Executors.newFixedThreadPool(15);
        //Проходим по всем строкам файла и создаем потоки, которые будут загружать строку по URL
        List<FutureTask<File>> tasks = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String line = reader.readLine();
            int count = 0;
            while (line != null) {
                line = reader.readLine();
                URL url = new URL(line);
                String fileName = "file" + count;
                Callable<File> callable = () -> URLFileProcessor.download(url, fileName);
                FutureTask<File> task = new FutureTask<>(callable);
                tasks.add(task);
                executor.submit(task);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Проверка потоков и вывод статуса загрузки в консоль
        List<File> files = new ArrayList<>();
        boolean allDone = false;
        while (!tasks.isEmpty()) {
            //Используем итератор поскольку он позволяет удаление во время обхода коллекции
            Iterator<FutureTask<File>> iterator = tasks.iterator();
            while (iterator.hasNext()) {
                FutureTask<File> task = iterator.next();
                if (task.isDone()) {
                    try {
                        //Если поток загрузил файл, то мы убираем его из списка Future,
                        //чтобы позволить пользователю сразу получить скачанные данные
                        files.add(task.get());
                        iterator.remove();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }
                //Интересное найденное мною решение через логическое И для проверки завершения всех задач в цикле
                //allDone &= task.isDone();
            }
        }
        System.out.println(files.stream().mapToLong(File::length).sum() / (1024 * 1024) + " mb");
        executor.shutdown();
    }
}
