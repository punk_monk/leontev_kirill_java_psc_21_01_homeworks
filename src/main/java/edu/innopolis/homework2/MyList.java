package edu.innopolis.homework2;

import java.util.Iterator;

public interface MyList<E>{
    void add(E element);
    E get(int index);
    E set(int index, E o);
    boolean isEmpty();
    Iterator<E> iterator();
    int size();
}
