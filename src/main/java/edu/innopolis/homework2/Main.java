package edu.innopolis.homework2;

import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        MyArrayList<Integer> array;
        array = new MyArrayList<>(Integer.class, 3);
        MyArrayList.MyIterator iterator = array.new MyIterator();
        array.add(45);
        array.add(56);
        array.add(12);
        System.out.println(array.size());
        array.add(99);
        System.out.println(array.size());
        array.set(0, 1001);
        while (iterator.hasNext()) System.out.println(iterator.next());
    }

}
