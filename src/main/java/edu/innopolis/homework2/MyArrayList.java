package edu.innopolis.homework2;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyArrayList<E> implements MyList<E>{

    private static final int DEFAULT_SIZE = 100;
    private static final int SIZE_MULTIPLIER = 2;
    private E[] my_array;
    private int size;
    private int currentIndex;

    //Не совсем Generic-подход, однако такая инициализация более безопасна
    MyArrayList(Class<E> datatype) {
        this.size = DEFAULT_SIZE;
        this.my_array = (E[]) Array.newInstance(datatype, DEFAULT_SIZE);
        this.currentIndex = 0;
    }

    MyArrayList(Class<E> datatype, int initialCapacity) {
        this.size = initialCapacity;
        this.my_array = (E[]) Array.newInstance(datatype, initialCapacity);
        this.currentIndex = 0;
    }

    @Override
    public void add(E o) {
        if (currentIndex == size) {
            //clone() -> использует первый конструктор
            E[] temp = my_array.clone();
            size *= SIZE_MULTIPLIER;
            my_array = (E[]) Array.newInstance(temp[0].getClass(), size);
            System.arraycopy(temp, 0, my_array, 0, temp.length);
        }
        my_array[currentIndex] = o;
        currentIndex++;
    }

    @Override
    public E set(int index, E o) {
        checkIndex(index);
        E old = my_array[index];
        my_array[index] = o;
        return old;
    }

    @Override
    public E get(int index) {
        checkIndex(index);
        return my_array[index];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new MyIterator();
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException("No such index");
        }
    }

    class MyIterator implements Iterator<E> {
        int current = 0;

        @Override
        public boolean hasNext() {
            return current < MyArrayList.this.currentIndex;
        }

        @Override
        public E next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            return my_array[current++];
        }
    }
}
