package edu.innopolis.homework6;

import java.util.NoSuchElementException;

public class UserNotFoundException extends NoSuchElementException {
    @Override
    public String getMessage() {
        return "Пользователь не найден";
    }
}

