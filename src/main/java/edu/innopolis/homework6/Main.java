package edu.innopolis.homework6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        IDGenerator idGenerator = new IDGeneratorFileImpl("users_id.txt");
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt", idGenerator);
        UsersService usersService = new UsersServiceImpl(usersRepository);

        Scanner scanner = new Scanner(System.in);

        while(true) {
            String email = scanner.nextLine();
            String password = scanner.nextLine();

            usersService.signUp(email, password);
        }
    }
}
