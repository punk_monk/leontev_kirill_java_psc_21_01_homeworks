package edu.innopolis.homework6;

public class User {
    private Integer id;
    private String email;
    private String password;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String asLine() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getId()).append("|").append(this.getEmail()).append("|").append(this.getPassword());
        return sb.toString();
    }
}
