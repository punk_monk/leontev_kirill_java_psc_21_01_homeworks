package edu.innopolis.homework6;

import java.util.List;
import java.util.Optional;

public interface UsersRepository {
    void update(User user);
    Optional<User> findByEmail(String email);
    List<User> findAll();
    void save(User user);
    void delete(User user);
    long count();
    boolean existsByEmail(String email);
}
