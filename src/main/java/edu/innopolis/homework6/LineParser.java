package edu.innopolis.homework6;

public class LineParser {
    private int id;
    private String email;
    private String password;

    public LineParser(String line) {
        String[] parts = line.split("\\|");
        this.id = Integer.parseInt(parts[0]);
        this.email = parts[1];
        this.password = parts[0];
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
