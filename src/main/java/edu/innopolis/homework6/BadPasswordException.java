package edu.innopolis.homework6;

public class BadPasswordException extends IllegalArgumentException{
    @Override
    public String getMessage() {
        return "Неверный формат пароля";
    }
}

