package edu.innopolis.homework6;

import java.io.*;
import java.util.Scanner;

public class IDGeneratorFileImpl implements IDGenerator {
    private final String fileName;

    public IDGeneratorFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public Integer next() {
        try (Scanner scanner = new Scanner(new FileInputStream(fileName))) {
            int lastID = scanner.nextInt();
            lastID++;
            scanner.close();
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(fileName))) {
                printWriter.print(lastID);
            }
            return lastID;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
