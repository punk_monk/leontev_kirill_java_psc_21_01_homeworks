package edu.innopolis.homework6;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsersServiceImpl implements UsersService{
    private final UsersRepository usersRepository;

    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    private boolean isEmailValid(String email) {
        //Email Validation Permitted by RFC 5322
        String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isPasswordValid(String password) {
        // digit + lowercase char + at least 7 symbols
        String regex = "^(?=.*[0-9])(?=.*[a-z]).{7,}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    @Override
    public void signUp(String email, String password) throws BadEmailException, BadPasswordException {
        if (!usersRepository.existsByEmail(email)) {
            if (!isEmailValid(email)) throw new BadEmailException();
            if (!isPasswordValid(password)) throw new BadPasswordException();
            User user = new User(email, password);
            usersRepository.save(user);
        } else throw new IllegalArgumentException("Email already exists");
    }

    @Override
    public void signIn(String email, String password) throws UserNotFoundException {
        if (usersRepository.existsByEmail(email)) {
            User user = new User(email, password);
            usersRepository.delete(user);
        }
        else throw new UserNotFoundException();

    }
}
