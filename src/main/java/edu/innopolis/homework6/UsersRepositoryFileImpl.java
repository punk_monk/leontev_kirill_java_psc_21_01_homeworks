package edu.innopolis.homework6;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class UsersRepositoryFileImpl implements UsersRepository {
    private final String filename;
    private final IDGenerator idGenerator;

    public UsersRepositoryFileImpl(String filename, IDGenerator idGenerator) {
        this.filename = filename;
        this.idGenerator = idGenerator;
    }

    @Override
    public void update(User user) {
        StringBuilder stringBuffer = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = reader.readLine();
            while (line != null) {
                Integer lineID = Integer.valueOf(line.split("\\|")[0]);
                if (lineID.equals(user.getId())) {
                    //Если встретили пользователя с тем же ID - переписываем строку буфера который заменит исходный файл
                    line = user.asLine();
                }
                stringBuffer.append(line);
                stringBuffer.append(System.getProperty("line.separator"));
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            writer.write(stringBuffer.toString());
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = reader.readLine();
            while(line != null) {
                LineParser lp = new LineParser(reader.readLine());
                String _email = lp.getEmail();
                if (_email.equals(email)) {
                    Integer id = lp.getId();
                    String password = lp.getPassword();
                    User user = new User(_email, password);
                    user.setId(id);
                    return Optional.of(user);
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new NoSuchElementException();
        }
        return Optional.empty();
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = reader.readLine();
            while (line != null) {
                LineParser lp = new LineParser(line);
                Integer id = lp.getId();
                String email = lp.getEmail();
                String password = lp.getPassword();
                User user = new User(email, password);
                user.setId(id);
                users.add(user);
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
        return users;
    }


    @Override
    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true))) {
            user.setId(idGenerator.next());
            writer.write(user.asLine());
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(User user) {
        StringBuilder stringBuffer = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = reader.readLine();
            while (line != null) {
                if (!line.equals(user.asLine())) {
                    stringBuffer.append(line);
                    stringBuffer.append(System.getProperty("line.separator"));
                    line = reader.readLine();
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            writer.write(stringBuffer.toString());
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public long count() {
        long count = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            count = reader.lines().count();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public boolean existsByEmail(String email) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = reader.readLine();
            while(line != null) {
                if (email.equals(line.split("\\|")[1])) return true;
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new NoSuchElementException();
        }
        return false;
    }
}

